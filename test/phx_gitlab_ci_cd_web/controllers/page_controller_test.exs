defmodule PhxGitlabCiCdWeb.PageControllerTest do
  use PhxGitlabCiCdWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Listing Products"
  end
end
