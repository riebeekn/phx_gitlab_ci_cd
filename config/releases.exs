import Config

secret_key_base = System.fetch_env!("SECRET_KEY_BASE")
port = String.to_integer(System.get_env("PORT") || "4000")
# DB ENV variables
db_host = System.fetch_env!("DB_HOST")
db_instance = System.fetch_env!("DB_INSTANCE")
db_user = System.fetch_env!("DB_USER")
db_password = System.fetch_env!("DB_PASSWORD")
db_port = String.to_integer(System.get_env("DB_PORT") || "5432")
db_ssl_enabled = String.to_existing_atom(System.get_env("DB_SSL_ENABLED") || "true")
db_pool_size = String.to_integer(System.get_env("DB_POOL_SIZE") || "10")

db_show_sens_info_on_error =
  String.to_existing_atom(System.get_env("DB_SHOW_SENS_INFO") || "false")

config :phx_gitlab_ci_cd, PhxGitlabCiCdWeb.Endpoint,
  url: [host: "example.com", port: 80],
  http: [:inet6, port: port],
  secret_key_base: secret_key_base,
  server: true

# Do not print debug messages in production
config :logger, level: :info

# Database config
config :phx_gitlab_ci_cd, PhxGitlabCiCd.Repo,
  hostname: db_host,
  username: db_user,
  password: db_password,
  database: db_instance,
  port: db_port,
  ssl: db_ssl_enabled,
  pool_size: db_pool_size,
  show_sensitive_data_on_connection_error: db_show_sens_info_on_error
