# Setting up a Phoenix CI / CD pipeline on GitLab

A simple Phoenix application demonstrating a GitLab CI / CD pipeline.

For more information see: [https://experimentingwithcode.com/setting-up-a-phoenix-ci-cd-pipeline-on-gitlab-part-1](https://experimentingwithcode.com/setting-up-a-phoenix-ci-cd-pipeline-on-gitlab-part-1)