defmodule PhxGitlabCiCdWeb.PageController do
  use PhxGitlabCiCdWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
