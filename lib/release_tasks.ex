defmodule PhxGitlabCiCd.ReleaseTasks do
  @moduledoc false
  def migrate do
    IO.puts("***** RUNNING MIGRATIONS *****")
    {:ok, _} = Application.ensure_all_started(:phx_gitlab_ci_cd)

    path = Application.app_dir(:phx_gitlab_ci_cd, "priv/repo/migrations")

    Ecto.Migrator.run(PhxGitlabCiCd.Repo, path, :up, all: true)
    IO.puts("***** FINISHED MIGRATIONS *****")
  end
end
